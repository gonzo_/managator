require 'action_mailer'

# mailer
class Mailer < ActionMailer::Base

  def commit_email(tasks)
    @tasks = tasks

    mail(
      to: 'abarrerac@gmail.com,jlopez@lemontech.cl,jsegovia@lemontech.cl',
      from: 'notificaciones@thecasetracking.com',
      subject: 'Compromiso Semanal'
    )
  end
end
